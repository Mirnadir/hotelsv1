'use strict';

$(document).ready(function () {
    // casino games tab --- [[
    $('#casino-games__tab--nav li:first-child').addClass('active');
    $('.tab-item').hide();
    $('.tab-item:first').show();

    $('#casino-games__tab--nav li').click(function () {
        $('#casino-games__tab--nav li').removeClass('active');
        $(this).addClass('active');
        $('.tab-item').hide();

        var activeTab = $(this).find('a').attr('href');
        $(activeTab).fadeIn();
        return false;
    });
    // casino games tab --- ]]

    // our score popup --- [[
    $(".ourScore-link").click(function () {
        $("body").css("overflow", "hidden");
        $(".our-score").css("display", "block");
    });

    $(".js-our-score").click(function () {
        $(".mobileMenu").css("display", "none");
        $("body").css("overflow", "hidden");
        $(".our-score").css("display", "block");
    });

    $(".close-modal").click(function () {
        $(".our-score").css("display", "none");
        $("body").css("overflow", "auto");
    });
    // our score popup --- ]]

    // starPopup --- [[
    $(".jq-star").click(function () {
        $("body").css("overflow", "hidden");
        $(".starPopup").css("display", "block");
    });

    $(".close-modal").click(function () {
        $(".starPopup").css("display", "none");
        $("body").css("overflow", "auto");
    });
    // starPopup --- ]]

    // mobile menu --- [[
    $(".c-header__main--mobileMenu img").click(function () {
        $("body").css("overflow", "hidden");
        $(".mobileMenu").css("display", "block");
    });

    $(".close-modal").click(function () {
        $(".mobileMenu").css("display", "none");
        $("body").css("overflow", "auto");
    });
    // mobile menu --- ]]

    // window functs --- [[
    var starPopup = $(".starPopup").get(0);
    var ourScore = $(".our-score").get(0);
    var mobileMenu = $(".mobileMenu").get(0);
    window.onclick = function (event) {
        if (event.target == starPopup) {
            $(".starPopup").css("display", "none");
            $("body").css("overflow", "auto");
        } else if (event.target == ourScore) {
            $(".our-score").css("display", "none");
            $("body").css("overflow", "auto");
        } else if (event.target == mobileMenu) {
            $(".mobileMenu").css("display", "none");
            $("body").css("overflow", "auto");
        }
    };
    // window functs --- ]]
});
//# sourceMappingURL=main.js.map
